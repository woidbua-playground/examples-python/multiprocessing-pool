from multiprocessing import Pool, current_process
import time
from random import randint

def calc_square(number):
    p = current_process()
    time.sleep(randint(0, 9))
    print("process:", p._identity[0], number)
    return number

if __name__ == "__main__":
    numbers = range(100)
    p = Pool(processes=5)
    result = p.map(calc_square, numbers)
    # result = p.map(calc_square, numbers, chunksize=10)
    # p.join()